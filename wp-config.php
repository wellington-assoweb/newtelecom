<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'newtelecom');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', '192.168.2.99');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L#)0JJ8$tW|5>A2{MK:hF-l*E[>|owC[_v z(E)Yg-W2`]{fJM|oCDCI^w9TmE=T');
define('SECURE_AUTH_KEY',  '|M<z*dbO8?_Enx:PGR%2{,L>3A&_3=kJIDyAv#y#]lO5ju~yi9J8z%jUr0b`-@AF');
define('LOGGED_IN_KEY',    '@lFT-Qqr.x>MoUwWv ~`j?7oJF|>?w/Xo1C;LW=k z$>|>RC>j*A&b8R!XXlwjsM');
define('NONCE_KEY',        'l_TUV_*04)Dv$|1x4OC x#%eEOnL o2%?1r52_fAal%go20De[4FXJ>LU(%KD`j?');
define('AUTH_SALT',        'G:a:Whk(IO?vkSSRkSz+5-=aM1XE)Nsj[Ac%uVHW6+!Ax7Bw[TDiqag[28z8=CE^');
define('SECURE_AUTH_SALT', 'hp$cU@*2Mm(8ASoyqNV?P !Zx#s4fEA*wy1m3^zd*Xp8q=5ZoY60_m/spwxUYU>v');
define('LOGGED_IN_SALT',   '7Eq%+ Dv[krw;((emj!+^U]h<z?<a>MSt_sH7j_Bu7XF7IPAaAQVRb$r-GtXN#1z');
define('NONCE_SALT',       't@Yz.9lY{nhsNRG~$.X]F}%^FDNRMM$]e(@e^E3Pcve !B;lP/ 99u_M(GlRG>Q%');

define('THEMEURL',   'http://localhost/newtelecom/wp-content/themes/new-telecom');
define('SITEURL',    'http://localhost/newtelecom');
/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'f6va51v_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);
define('FS_METHOD','direct');

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');