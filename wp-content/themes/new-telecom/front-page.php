<?php /* Template name: Página Inicial */ get_header(); ?>
<section class="banner">
	<div id="muteYouTubeVideoPlayer"></div>

	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="group">
					<h2>A MELHOR CONEXÃO PARA VOCÊ.</h2>
					<h3>ASSINE AGORA!</h3>
					<div class="box-preco">
						<div class="box-mega">
							<span class="qtd-mega">10</span>
							<span class="mega">mega</span>
						</div>
						<div class="box-mais">
							<span>+</span>
						</div>
						<div class="box-wifi-gratis">
							<span>WI-FI</span><br/>
							<span>GRÁTIS</span>
						</div>
						<div class="box-reais">
							<span class="igual">=</span>
							<span class="cifrao">R$</span>
							<span class="reais">59</span>
							<span class="centavos">,90</span>
						</div>
						<br>
						<div class="botao btn-branco">
							<a href="<?php //echo SITEURL ?>/solucoes">
								<span>Clique AQUI e aproveite a promoção!</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="#" class="seta-down">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
				 y="0px" viewBox="0 0 102 102" style="enable-background:new 0 0 102 102;" xml:space="preserve">
			<g>
				<path class="circle" d="M51,0c28.1,0,51,22.9,51,51c0,28.1-22.9,51-51,51C22.9,102,0,79.1,0,51C0,22.9,22.9,0,51,0z M51,97.3
					c25.5,0,46.3-20.8,46.3-46.3C97.3,25.4,76.5,4.6,51,4.6C25.4,4.6,4.6,25.4,4.6,51C4.6,76.6,25.4,97.3,51,97.3z"/>
				<path d="M51,10.4c22.4,0,40.6,18.2,40.6,40.6c0,22.4-18.2,40.6-40.6,40.6c-22.4,0-40.6-18.2-40.6-40.6
					C10.4,28.6,28.6,10.4,51,10.4z M51,86.9c19.8,0,35.9-16.1,35.9-35.9c0-19.8-16.1-35.9-35.9-35.9C31.2,15.1,15,31.2,15,51
					C15,70.8,31.2,86.9,51,86.9z"/>
				<path class="seta" d="M30.9,39c0.7,0,1.4,0.3,1.8,0.9l18.6,24.6L70,39.9c0.8-1,2.2-1.2,3.3-0.4c1,0.8,1.2,2.2,0.4,3.2l-20.5,27
					c-0.9,1.2-2.8,1.2-3.7,0L29,42.7c-0.8-1-0.6-2.5,0.5-3.2C29.9,39.2,30.4,39,30.9,39z"/>
			</g>
		</svg>
	</a>
</section>

<section class="vantagens">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="col-xs-12 col-lg-offset-2 col-lg-9">
					<h2>Conheça as Vantagens</h2>
					<h3>Fibra Óptica de<br>verdade.</h3>
				</div>
				<div class="botao-circle">
					<a href="#">
						<div class="box-svg-label">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="-218 308.6 177.5 177.4" xml:space="preserve">
								<g class="int">
									<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
								</g>
								<g class="mei">
									<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
								</g>
								<g class="ext">
									<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
									<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
								</g>
							</svg>
							<div class="label">ASSINE<br>AGORA</div>
						</div>
					</a>
				</div>
				<div class="botao">
					<a href="#">
						<span>Ou conheça melhor a NEW!</span>
					</a>
				</div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-wifi.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-game.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-notebook.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-midia.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-notebook.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6">
					<div class="item">
						<img src="<?php echo THEMEURL ?>/assets/img/icon-midia.svg" alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>
<section class="para-voce">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-6 col-md-6 col-lg-offset-7 col-lg-5">
				<div class="box">
					<h3>Para sua casa!</h3>
					<h4>Assista filmes, jogue online, navegue sem limites!</h4>
					<div class="box-preco">
						<div class="box-mega">
							<span class="qtd-mega">10</span>
							<span class="mega">mega</span>
						</div>
						<div class="box-mais">
							<span>+</span>
						</div>
						<div class="box-wifi-gratis">
							<span>WI-FI</span><br/>
							<span>GRÁTIS</span>
						</div>
						<div class="box-reais">
							<span class="igual">=</span>
							<span class="cifrao">R$</span>
							<span class="reais">59</span>
							<span class="centavos">,90</span>
						</div>
						<br>
					</div>
					<div class="botao-circle">
						<a href="#">
							<div class="box-svg-label">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="-218 308.6 177.5 177.4" xml:space="preserve">
									<g class="int">
										<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
									</g>
									<g class="mei">
										<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
									</g>
									<g class="ext">
										<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
										<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
									</g>
								</svg>
								<div class="label">ASSINE<br>AGORA</div>
							</div>
						</a>
					</div>
					<div class="botao">
						<a href="#">
							<span>Ou conheça outros planos!</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="para-empresa">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-offset-1 col-md-5">
				<div class="box">
					<h3>Para sua casa!</h3>
					<h4>Assista filmes, jogue online, navegue sem limites!</h4>
					<div class="box-preco">
						<div class="box-mega">
							<span class="qtd-mega">10</span>
							<span class="mega">mega</span>
						</div>
						<div class="box-mais">
							<span>+</span>
						</div>
						<div class="box-wifi-gratis">
							<span>WI-FI</span><br/>
							<span>GRÁTIS</span>
						</div>
						<div class="box-reais">
							<span class="igual">=</span>
							<span class="cifrao">R$</span>
							<span class="reais">59</span>
							<span class="centavos">,90</span>
						</div>
						<br>
					</div>
					<div class="botao-circle">
						<a href="#">
							<div class="box-svg-label">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
										 viewBox="-218 308.6 177.5 177.4" xml:space="preserve">
									<g class="int">
										<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
									</g>
									<g class="mei">
										<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
									</g>
									<g class="ext">
										<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
										<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
									</g>
								</svg>
								<div class="label">ASSINE<br>AGORA</div>
							</div>
						</a>
					</div>
					<div class="botao">
						<a href="#">
							<span>Ou conheça outros planos!</span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 imagem-para-empresa">
				<img src="<?php echo THEMEURL ?>/assets/img/para-empresa.jpg" alt="">
			</div>
		</div>
	</div>
</section>

<section class="comparador"></section>

<section class="jogue">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12 col-md-7">
				<div class="box">
					<h3>Jogue sem interrupções!</h3>
					<p>O melhor da ultra banda larga em suas mãos, é só na NEW!</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-5">
				<div class="botao-circle">
					<a href="#">
						<div class="box-svg-label">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="-218 308.6 177.5 177.4" xml:space="preserve">
								<g class="int">
									<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
								</g>
								<g class="mei">
									<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
								</g>
								<g class="ext">
									<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
									<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
								</g>
							</svg>
							<div class="label">ASSINE<br>AGORA</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>