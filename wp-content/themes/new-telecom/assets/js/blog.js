(function($) {

    jQuery(document).ready(function($) {

        // aumentando a fonte
        var conteudoArtigo = $('#conteudo-artigo-geral');
        conteudoArtigo.hover(
            function() {
                $('.sidebar').addClass('is-opacity');
                $('.sidebar-right').addClass('is-opacity');
            }, function() {
                $('.sidebar').removeClass('is-opacity');
                $('.sidebar-right').removeClass('is-opacity');
            }
        );

        // aumentando a fonte
        $(".inc-font").click(function(event) {
            event.preventDefault();
            $('#conteudo-artigo').each(function(){
                var size = $(this).children('p').css('font-size');
                console.log(size);
                size = size.replace('px', '');
                size = parseInt(size) + 1.8;

                $(this).children('p').animate({'font-size' : size + 'px'});
            });
        });

        //diminuindo a fonte
        $(".dec-font").click(function(event) {
            event.preventDefault();
            $('#conteudo-artigo').each(function(){
                var size = $(this).children('p').css('font-size');
                console.log(size);
                size = size.replace('px', '');
                size = parseInt(size) - 1.8;

                $(this).children('p').animate({'font-size' : size + 'px'});
            });

        });

        // resetando a fonte
        $(".res-font").click(function(event) {
            event.preventDefault();
            $('#conteudo-artigo').each(function(){
                $(this).children('p').animate({'font-size' : '16px'});
            });
        });


        var menu = $('.menu-blog .nav-blog .blog-primary-nav');
        var submenu = $('.menu-blog .nav-blog ul.blog-primary-nav li.menu-item-has-children ul');
        var hasChildren = $('.menu-blog .nav-blog ul.blog-primary-nav li.menu-item-has-children');
        submenu.addClass('is-hidden');

        $('.menu-blog .nav-blog .blog-primary-nav').children('.menu-item-has-children').children('a').on('click', function(event){
            event.preventDefault();
        });
        //open submenu
        hasChildren.children('a').on('click', function(event){
            //if( !checkWindowWidth() ) event.preventDefault();
            var selected = $(this);
            if( selected.next('ul').hasClass('is-hidden') ) {
                //desktop version only
                selected.addClass('selected').next('ul').removeClass('is-hidden').addClass('shownow').end().parent('.menu-item-has-children').parent('ul').addClass('moves-out');
                selected.parent('.menu-item-has-children').siblings('.menu-item-has-children').children('ul').addClass('is-hidden').removeClass('shownow').end().children('a').removeClass('selected');
            } else {
                selected.removeClass('selected').next('ul').addClass('is-hidden').removeClass('shownow').end().parent('.menu-item-has-children').parent('ul').removeClass('moves-out');
            }
        });
    });



})(jQuery);
