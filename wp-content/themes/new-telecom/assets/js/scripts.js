(function($) {

    // UPDATE YOUR BROWSER
    var $buoop = {vs:{i:11,f:30,o:15,s:7},c:2};
    function $buo_f(){
        var e = document.createElement("script");
        e.src = "//browser-update.org/update.min.js";
        document.body.appendChild(e);
    };
    try {
        document.addEventListener("DOMContentLoaded", $buo_f,false)
    }catch(e){
        window.attachEvent("onload", $buo_f)
    }
    // END UPDATE YOUR BROWSER

    //SMALLER HEADER WHEN SCROLL PAGE
    $(window).scroll(function () {
         var sc = $(window).scrollTop()
        if (sc > 40) {
            $("#header-sroll").addClass("small")
        }else {
            $("#header-sroll").removeClass("small")
        }
    });

    //CLOSE WHEN PRESS ESC
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if($(".mask").hasClass("active")){
                closeModal();
            }
        }
    });

    //HAMBURGER MENU ADDCLASS FOR ANIMATE
    var openbtn = document.getElementById( 'open-button' ),
        isOpen = false,
        $headerSroll = $('#header-sroll'),
        $bar = $('.bar');

    $('.hamburger-menu').on('click', function() {
        $bar.toggleClass('animate');
        $headerSroll.toggleClass('show-menu');
    })

       /* MENU SCRIPT */
    var openbtn = document.getElementById( 'open-button' ),
        headscroll = document.getElementById( 'header-sroll' ),
        isOpen = false;

    function init() {
        initEvents();
    }

    function initEvents() {
        openbtn.addEventListener( 'click', toggleMenu );
    }
    function toggleMenu() {
        if( isOpen ) {
            classie.remove( headscroll, 'show-menu' );
        }
        else {
            classie.add( headscroll, 'show-menu' );
        }
        isOpen = !isOpen;
    }
    init();

    //KEEP THE FOOTER IN FOOTER
    // function rodape(){
    //     var footerHeight = $('.footer').height();
    //     $('.footer').css('margin-top', -(footerHeight)+"px");
    //     $('.conteudo').css('padding-bottom', (footerHeight + 80)+"px");
    // };

    jQuery(document).ready(function(){
        rodape();
    });

    jQuery(window).resize(function() {
        rodape();
    });

    $(".login").click(function(){
          $('html,body').animate({scrollTop:$('#login').offset().top}, 900);
          $("#login").css('animation','scale 2s');
    });

})(jQuery);