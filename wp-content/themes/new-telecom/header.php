<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#0072BC"><!-- Define a cor da janela para android -->
		<title><?php the_title();?></title>

		<!--Favicon-->
		<link rel="shortcut icon" href="<?php echo THEMEURL; ?>/assets/img/favicon.png" >
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEMEURL; ?>/assets/img/apple-touch-icon-152x152.png">

		<?php //include(THEMEURL . '/template-parts/loop-style.php'); ?>
		<?php wp_head(); ?>


		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if IE ]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<link rel="stylesheet" type="text/css" href="ie.css" />
		<![endif]-->
	</head>
	<body <?php body_class(); ?>>
		<div id="wrap" class="cd-main-content">
			<header id="header-sroll">
				<div class="header">
					<div class="my-container">
						<div class="row">
							<div class="col-xs-12">
								<div class="desk-menu">
									<div class="logo">
										<?php
											if(is_front_page()): ?>
												<h1 class="logo-adn">
													<a class="siteLogo" data-g-label="New Telecom" title="New Telecom" href="<?php echo SITEURL; ?>">New Telecom</a>
												</h1>
											<?php else: ?>
												<span class="logo-adn">
													<a class="siteLogo" data-g-label="New Telecom" title="New Telecom" href="<?php echo SITEURL; ?>">New Telecom</a>
												</span>
											<?php endif;
										?>
									</div>
									<nav class="box-menu">
							      		<ul id="menu-header">
									      	<li>
									      		<a href="#">
											      	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															 viewBox="-218 308.6 177.5 177.4" style="enable-background:new -218 308.6 177.5 177.4;" xml:space="preserve">
														<g class="int">
															<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
														</g>
														<g class="mei">
															<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
														</g>
														<g class="ext">
															<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
															<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
														</g>
														<g class="desen">
															<path class="st1" d="M-107.6,433.4h-42.5c-0.9,0-1.6-0.7-1.6-1.6v-36.9h-8c-0.7,0-1.2-0.4-1.5-1c-0.2-0.6-0.1-1.3,0.4-1.8l31.2-29
																c0.6-0.6,1.6-0.6,2.2,0l31,29c0.5,0.4,0.6,1.1,0.4,1.7c-0.2,0.6-0.8,1-1.5,1h-8.4v36.9C-106,432.6-106.7,433.4-107.6,433.4z
																 M-148.5,430.2h39.3v-36.9c0-0.9,0.7-1.6,1.6-1.6h6l-27-25.2l-27.1,25.2h5.6c0.9,0,1.6,0.7,1.6,1.6V430.2z"/>
															<path class="st1" d="M-122.4,432.9c-0.9,0-1.6-0.7-1.6-1.6v-25.2h-9.3v25.2c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6v-26.8
																c0-0.9,0.7-1.6,1.6-1.6h12.5c0.9,0,1.6,0.7,1.6,1.6v26.8C-120.9,432.2-121.6,432.9-122.4,432.9z"/>
															<path class="st1" d="M-109.2,382.4c-0.9,0-1.6-0.7-1.6-1.6v-15.9h-2.9v10.4c0,0.9-0.7,1.6-1.6,1.6c-0.9,0-1.6-0.7-1.6-1.6v-12
																c0-0.9,0.7-1.6,1.6-1.6h6c0.9,0,1.6,0.7,1.6,1.6v17.5C-107.6,381.7-108.3,382.4-109.2,382.4z"/>
														</g>
													</svg><br/>
													<span>PLANOS RESIDENCIAIS</span>
												</a>
											</li>
											<li>
												<a href="#">
													<svg version="1.1" id="icon-planos-empre" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-218 308.6 177.5 177.4" style="enable-background:new -218 308.6 177.5 177.4;" xml:space="preserve">
														<g class="int">
															<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
														</g>
														<g class="mei">
															<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
														</g>
														<g class="ext">
															<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
															<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
														</g>
														<g class="desen">
															<path d="M-99.1,433.3h-59.1c-0.9,0-1.6-0.7-1.6-1.6v-37.3c0-0.8,0.5-1.5,1.3-1.7c1-0.1,1.8,0.6,1.8,1.5v35.9c0,0,0,0,0,0h56
																c0,0,0,0,0,0v-35.8c0-0.6,0.3-1.2,0.9-1.5c1.1-0.5,2.2,0.4,2.2,1.4v37.4C-97.5,432.6-98.2,433.3-99.1,433.3z"/>
															<path d="M-93.1,386.6c-0.7,0-1.4-0.5-1.5-1.2l-4.6-20.5c0,0,0,0,0,0H-158c0,0,0,0,0,0l-4.6,20.5c-0.2,0.8-1,1.4-1.9,1.2
																c-0.8-0.2-1.4-1-1.2-1.9l4.9-21.7c0.2-0.7,0.8-1.2,1.5-1.2H-98c0.7,0,1.4,0.5,1.5,1.2l4.9,21.7c0.2,0.8-0.3,1.7-1.2,1.9
																C-92.9,386.6-93,386.6-93.1,386.6z"/>
															<g>
																<path d="M-155.4,395.6c-5.7,0-10.3-4.6-10.3-10.4c0-0.7,0.3-1.3,0.9-1.5c1.1-0.4,2.2,0.4,2.2,1.4c0,4,3.3,7.4,7.2,7.4
																	c4,0,7.3-3.4,7.3-7.4c0-1,1.1-1.8,2.2-1.4c0.6,0.2,0.9,0.9,0.9,1.5C-145,390.8-149.8,395.6-155.4,395.6z"/>
																<path d="M-137.5,395.6c-5.7,0-10.5-4.8-10.5-10.5c0-0.9,0.8-1.7,1.8-1.5c0.7,0.1,1.3,0.8,1.3,1.6c0,4,3.4,7.4,7.4,7.4
																	c3.9,0,7.3-3.4,7.3-7.4c0-0.8,0.5-1.4,1.3-1.6c1-0.2,1.8,0.6,1.8,1.5C-127.1,390.7-131.9,395.6-137.5,395.6z"/>
																<path d="M-119.7,395.6c-5.7,0-10.4-4.7-10.5-10.4c0-0.7,0.3-1.3,0.9-1.5c1.1-0.4,2.2,0.4,2.2,1.4c0,4,3.4,7.4,7.4,7.4
																	c3.9,0,7.3-3.4,7.3-7.4c0-0.8,0.5-1.4,1.3-1.6c1-0.2,1.8,0.6,1.8,1.5C-109.3,390.7-114.1,395.6-119.7,395.6z"/>
																<path d="M-101.9,395.6c-5.7,0-10.4-4.8-10.5-10.4c0-0.7,0.3-1.3,0.9-1.5c1.1-0.4,2.2,0.4,2.2,1.4c0,4,3.4,7.4,7.4,7.4
																	c3.9,0,7.2-3.4,7.2-7.4c0-0.9,0.7-1.5,1.6-1.5c0.9,0,1.5,0.9,1.5,1.7C-91.7,391-96.3,395.6-101.9,395.6z"/>
															</g>
															<path d="M-146.5,386.6c-0.1,0-0.1,0-0.2,0c-0.8-0.1-1.5-0.9-1.4-1.7l2.3-21.5c0.1-0.9,0.9-1.5,1.7-1.4c0.9,0.1,1.5,0.9,1.4,1.7
																l-2.4,21.5C-145.1,386-145.7,386.6-146.5,386.6z"/>
															<path d="M-128.7,386.6c-0.9,0-1.6-0.7-1.6-1.5l-0.1-21.5c0-0.9,0.8-1.7,1.5-1.6c0.9,0,1.6,0.7,1.6,1.5l0.1,21.5
																C-127.1,385.9-127.8,386.6-128.7,386.6L-128.7,386.6z"/>
															<path d="M-110.9,386.6c-0.8,0-1.5-0.6-1.5-1.4l-2.5-21.5c-0.1-0.8,0.5-1.6,1.4-1.7c0.8-0.1,1.6,0.5,1.7,1.4l2.5,21.5
																c0.1,0.8-0.5,1.6-1.4,1.7C-110.7,386.6-110.8,386.6-110.9,386.6z"/>
															<path d="M-136.3,432.5c-0.7-0.1-1.2-0.9-1.2-1.6l0-23.1c0,0,0,0,0,0h-10.3c0,0,0,0,0,0V431c0,0.8-0.7,1.5-1.6,1.5
																c-0.9,0-1.5-0.7-1.5-1.5v-24.8c0-0.9,0.7-1.6,1.5-1.6h13.4c0.9,0,1.6,0.7,1.6,1.6V431C-134.5,431.9-135.3,432.7-136.3,432.5z"/>
															<path d="M-105.8,409.5h-22.9c-0.9,0-1.6-0.7-1.6-1.6v-7.5c0-0.8,0.7-1.5,1.5-1.5h23c0.9,0,1.6,0.7,1.6,1.6v7.4
																C-104.2,408.8-104.9,409.5-105.8,409.5z M-127.2,406.4h19.8c0,0,0,0,0,0v-4.3c0,0,0,0,0,0h-19.8c0,0,0,0,0,0L-127.2,406.4
																C-127.2,406.4-127.2,406.4-127.2,406.4z"/>
														</g>
													</svg>
													<br/>
													<span>PLANOS EMPRESARIAIS</span>
												</a>
											</li>
											<li>
												<a href="#">
												<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
												 viewBox="-218 308.6 177.5 177.4" style="enable-background:new -218 308.6 177.5 177.4;" xml:space="preserve">
													<g class="int">
														<path class="st0" d="M-198.3,397.5c0-38.5,31.2-69.7,69.7-69.7s69.7,31.2,69.7,69.7"/>
													</g>
													<g class="mei">
														<path class="st0" d="M-128.6,475.3c-43,0-77.8-34.9-77.8-77.8s34.8-77.9,77.8-77.9s77.8,34.9,77.8,77.8"/>
													</g>
													<g class="ext">
														<path class="st0" d="M-41.4,397.5c0,48.2-39.1,87.2-87.2,87.2"/>
														<path class="st0" d="M-215.9,397.5c0-48.2,39.1-87.2,87.2-87.2"/>
													</g>
													<g class="desen">
														<path d="M-94.1,440.1h-67c-1.1,0-1.9-0.9-1.9-1.9l0-1.4c0-7.3-0.1-20.9,6.5-26.5c4.8-4.1,11.4-6.2,15.7-7.6
															c1.5-0.5,2.7-0.8,3.5-1.2c0.2-0.2,0.6-1.3,0.8-2.6c-7.6-3.6-12.7-12-12.7-21.3c0-12.6,9.9-23.2,21.6-23.2
															c11.7,0,21.6,10.6,21.6,23.2c0,9.3-5,17.6-12.7,21.3c0.2,1.2,0.6,2.4,1,2.6c0.7,0.3,1.9,0.7,3.4,1.2c4.4,1.4,11,3.4,15.7,7.6
															c6.6,5.7,6.5,19.2,6.5,26.5l0,1.4C-92.2,439.2-93,440.1-94.1,440.1z M-159.2,436.2H-96c0-6.4-0.1-18.7-5.1-23.1
															c-4.2-3.6-10.3-5.6-14.4-6.8c-1.7-0.5-3-1-3.9-1.4c-2.5-1.1-3.1-5.3-3.3-7.1c-0.1-0.8,0.4-1.7,1.2-2c7-2.7,11.6-10,11.6-18.3
															c0-10.5-8.1-19.3-17.7-19.3c-9.6,0-17.7,8.8-17.7,19.3c0,8.3,4.7,15.6,11.6,18.3c0.8,0.3,1.3,1.1,1.2,2c-0.2,1.7-0.8,5.9-3.3,7.1
															c-0.9,0.4-2.3,0.9-3.9,1.4c-4,1.3-10.2,3.2-14.4,6.8C-159.1,417.5-159.2,429.8-159.2,436.2z"/>
														<path d="M-128.3,392.2c-6.3-0.8-7.3-6.9-6.9-10c0.1-0.8,0.8-1.3,1.6-1.3c0.8,0.1,1.4,0.8,1.3,1.6c0,0.3-0.6,6.2,4.2,6.9l1.2,0
																c4.7-0.6,4.1-6.6,4.1-6.8c-0.1-0.8,0.5-1.5,1.3-1.6c0.8-0.1,1.5,0.5,1.6,1.3c0.3,3.1-0.6,9.2-6.7,10L-128.3,392.2z"/>
														<g>
															<circle cx="-135.3" cy="370.6" r="2.6"/>
															<circle cx="-119.8" cy="370.6" r="2.6"/>
														</g>
													</g>
												</svg>
												<br/>
												<span>ÁREA DO ASSINANTE</span>
												</a>
											</li>
										</ul>
									</nav>
									<div class="tel-header">
										<span><i class="icon-phone-squared"></i> (31) 3333-4444</span>
									</div>
									<div class="hamburger-menu" id="open-button">
		  								<div class="bar"></div>
		  							</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="conteudo">