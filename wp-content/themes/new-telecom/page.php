<?php get_header();?>
<section class="contact-info">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>