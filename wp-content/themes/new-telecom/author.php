<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php');
$author_id = get_the_author_meta('ID');?>
<div class="home-blog">
    <div class="container">
        <div class="row">
            <div class="hidden-xs hidden-sm col-md-4 col-lg-3">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9 author">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="authority">
                            <?php echo get_avatar( get_the_author_meta( 'user_email' ), 490 ); ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <h2><?php the_author(); ?></h2>
                    </div>
                    <div class="col-sm-12">
                        <?php if(get_field('about_author', 'user_'. $author_id )) :
                        echo get_field('about_author', 'user_'. $author_id );
                        endif;?>
                    </div>
                    <div class="col-sm-12 authorposts">
                    <?php
                    $args2 = array(
                        'author' => $author_id
                    );
                    $ultimos_posts = new WP_Query( $args2 );

                    if($ultimos_posts->have_posts()){?>
                        <div class="row">
                            <div class="col-xs-12">
                                <h3>Publicações</h3>
                            </div>
                        </div>
                        <div class="row">

                            <?php while( $ultimos_posts->have_posts() ) {
                            $ultimos_posts->the_post();?>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="itemArticle">
                                <?php
                                    $img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                                ?>
                                    <figure class="effect-lily">
                                        <?php if ($img_post_art[0]){?>
                                            <img src="<?php echo $img_post_art[0] ?>" alt="<?php the_title(); ?>"/>
                                        <?php }else{?>
                                            <img src="<?php echo THEMEURL?>/assets/img/default.jpg" alt="<?php the_title(); ?>"/>
                                        <?php }?>
                                        <figcaption>
                                            <div>
                                                <h4><?php the_title(); ?></h4>
                                                <p>Ler mais</p>
                                            </div>
                                            <a href="<?php the_permalink(); ?>">View more</a>
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                            <?php }?>
                        </div>
                        <?php }?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>