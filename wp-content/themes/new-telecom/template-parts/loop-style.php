<?php if (is_front_page()): ?>
	<style>
		<?php if( have_rows('slide') ):
		    while ( have_rows('slide') ) : the_row();
		        if( get_row_layout() == 'item_slide' ):
		        	$background = get_sub_field('imagem_fundo');	
					echo '.bg-slide-'.$background['name'].'{background-image: url("'.$background['url'].'")}' 
				endif;
			endwhile;
		endif; ?>
	</style>
<?php elseif(is_page()): ?>

<?php endif; ?>