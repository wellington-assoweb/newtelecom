<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>
<div class="home-blog">
    <div class="my-container">
        <div class="row">
            <div class="hidden-xs col-sm-6 col-md-4 col-lg-3">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9 resultados-busca">
			<?php $s = isset($_GET["s"]) ? $_GET["s"] : "";
			$solucoes = new WP_Query("s=$s&post_type=solucoes");
			$posts = new WP_Query("s=$s&post_type=post");
			$qtd_solucoes = $solucoes->found_posts;
			$qtd_posts = $posts->found_posts;


			function relevanssi_didyoumean($query, $pre, $post, $n = 5) {
				global $wpdb, $relevanssi_variables, $wp_query;

				$total_results = $wp_query->found_posts;
				if ($total_results > $n) return;

				$q = "SELECT query, count(query) as c, AVG(hits) as a FROM " . $relevanssi_variables['log_table'] . " WHERE hits > 1 GROUP BY query ORDER BY count(query) DESC";
				$q = apply_filters('relevanssi_didyoumean_query', $q);

				$data = $wpdb->get_results($q);

				$distance = -1;
				$closest = "";

				foreach ($data as $row) {
					if ($row->c < 2) break;
					$lev = levenshtein($query, $row->query);

					if ($lev < $distance || $distance < 0) {
						if ($row->a > 0) {
							$distance = $lev;
							$closest = $row->query;
							if ($lev == 1) break; // get the first with distance of 1 and go
						}
					}
				}

				if ($distance > 0) {

			 		$url = SITEURL;
					$url = esc_attr(add_query_arg(array(
						's' => urlencode($closest)

						), $url ));
					$url = apply_filters('relevanssi_didyoumean_url', $url);
					echo "$pre<a href='$url&post_type=post'>$closest</a>$post";
			 	}
			}


			if($qtd_solucoes == 0 && $qtd_posts == 0){?>
				<div class="row ss">
					<div class="col-lg-12">
						<h2>Infelizmente não encontramos nada para <span><?php echo $_GET["s"] ?></span></h2>
					</div>
				</div>
			<?php }else{
				if ( $solucoes->have_posts() ) :?>
					<div class="ss other-solutions">
						<div class="row">
							<div class="col-lg-12">
								<h2>Soluções encontradas para: <span><?php echo $_GET["s"] ?></span></h2>
							</div>
						</div>
						<div class="row">
							<?php
							while ($solucoes->have_posts()) : $solucoes->the_post();?>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
								<?php $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$url_img = $img_post['0']; ?>
							    <div class="one-solution" style="background-image:url('<?php echo $url_img ?>')">
									<div class="overlay"></div>
									<div class="box">
										<div class="title">
											<h3><?php echo $post->post_title; ?></h3>
										</div>
										<div class="preview">
											<p><?php the_content_limit(50); ?></p>
										</div>
										<div class="know">
											<a href="<?php the_permalink(); ?>">Conhecer</a>
										</div>
									</div>
								</div>
							</div>
						    <?php endwhile; ?>
						</div>
					</div>
				<?php
				wp_reset_postdata();
				endif;
				if( $posts->have_posts() ) :?>
					<div class="row">
						<div class="col-lg-12">
							<h2>Posts encontradas para: <span><?php echo $_GET["s"] ?></span></h2>
						</div>
					</div>
					<div class="row">
					<?php
					while ($posts->have_posts()) : $posts->the_post();?>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
	                        <div class="itemArticle">
		                        <?php
		                            $img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
		                        ?>
	                            <figure class="effect-lily">
	                                <?php if ($img_post_art[0]){?>
	                                    <img src="<?php echo $img_post_art[0] ?>" alt="<?php the_title(); ?>"/>
	                                <?php }else{?>
	                                    <img src="<?php echo THEMEURL?>/assets/img/default.jpg" alt="<?php the_title(); ?>"/>
	                                <?php }?>
	                                <figcaption>
	                                    <div>
	                                        <h4><?php echo $post->post_title; ?></h4>
                                        	<p>Ler mais</p>
	                                    </div>
	                                    <a href="<?php the_permalink(); ?>">Ver mais</a>
	                                </figcaption>
	                            </figure>
	                        </div>
	                    </div>
					    <?php endwhile; ?>
					</div>
				<?php
				wp_reset_postdata();
				endif;
				}
			?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>