<?php
/*=========================================================================================
LOAD SCRIPTS AND STYLES
=========================================================================================*/
add_action('wp_enqueue_scripts', 'sample_scripts');
function sample_scripts() {
  //PEGA O DIRETÓRIO DEFAULT DOS ARQUIVOS PARA CONCATENAR
  define(CSS_PATH, THEMEURL.'/assets/css/');
  define(JS_PATH, THEMEURL.'/assets/js/');

  wp_enqueue_script('jquery');

  wp_register_style('animate-css', CSS_PATH.'animate.min.css', null, null, 'all' );


  //Declaração de scripts
  wp_register_script('bootstrap_js', JS_PATH.'bootstrap.js', null, null, true );
  wp_register_script('mailchimp','//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', null, null, true );
  wp_register_script('smooth-scroll', JS_PATH.'smooth-scroll.js', null, null, true );
  wp_register_script('wow-js', JS_PATH.'wow.min.js', null, null, true );

  wp_register_script('scripts', JS_PATH.'scripts.js', null, null, true );

  wp_enqueue_script('bootstrap_js');
  //wp_enqueue_script('mailchimp');
  wp_enqueue_script('smooth-scroll');
  wp_enqueue_style('animate-css');


  /*FIM MENU*/
  if(is_front_page()){
    wp_register_style('front-page-css', CSS_PATH.'front-page.css', null, null, 'all' );
    wp_enqueue_style('front-page-css');

    wp_register_script('front-page-js', JS_PATH.'front-page.js', null, null, true );
    wp_enqueue_script('front-page-js');


  }elseif(is_page()){

    if(is_page(36)){ // SOBRE
      wp_register_style('sobre-css', CSS_PATH.'sobre.css', null, null, 'all' );
      wp_enqueue_style('sobre-css');
    }
    if(is_page(101)){ // SOLUÇÕES
      wp_register_style('solucoes-css', CSS_PATH.'solucoes.css', null, null, 'all' );
      wp_enqueue_style('solucoes-css');
    }
    if(is_page(114)){ // CLIENTES
      wp_register_style('clientes-css', CSS_PATH.'clientes.css', null, null, 'all' );
      wp_enqueue_style('clientes-css');
    }
    if(is_page(118)){ // CONTATO
      wp_register_style('contato-css', CSS_PATH.'contato.css', null, null, 'all' );
      wp_enqueue_style('contato-css');
    }

  }elseif(is_post_type_archive() || is_singular()){

    if (is_post_type_archive() || is_singular()) {
    }

    if (is_singular()) {}

  }elseif(is_home() || is_category() || is_single() || is_author() || is_search()){
    wp_register_style('blog-css', CSS_PATH.'blog.css', null, null, 'all' );
    wp_enqueue_style('blog-css');

    if(is_search()){
      wp_register_style('search-css', CSS_PATH.'search.css', null, null, 'all' );
      wp_enqueue_style('search-css');

    }elseif(is_single()){

      // DISQUS
      function disqus_embed($disqus_shortname) {
        global $post;
        wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
        echo '<div id="disqus_thread"></div>
        <script type="text/javascript">
          var disqus_shortname = "'.$disqus_shortname.'";
          var disqus_title = "'.$post->post_title.'";
          var disqus_url = "'.get_permalink($post->ID).'";
          var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
        </script>';
      }
    }

    wp_register_script('blog-js', JS_PATH.'blog.js', null, null, true );
    wp_enqueue_script('blog-js');

  }elseif(is_404()){
    wp_register_style('404-css', CSS_PATH.'404.css', null, null, 'all' );
    wp_enqueue_style('404-css');

  }

  //Chamada de scripts
  wp_enqueue_script('scripts');
  wp_enqueue_script('wow-js');
}



/*Deregister styles and scripts
================================================================================================================*/
function deregister_styles() {

    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_style('jetpack-whatsapp');
        wp_deregister_style( 'wp-pagenavi' );
    }
    if (!is_page('contato')) {
        wp_deregister_style( 'contact-form-7' );
    }
}
add_filter( 'wp_print_styles', 'deregister_styles' );

//Deregister styles and scripts
//================================================================================================================
function deregister_scripts() {
    if (!is_page('contato')) {
        wp_deregister_script( 'contact-form-7' );
    }
    if (!is_home() && !is_single() && !is_category() && !is_tag() && !is_author()) {
        wp_deregister_script('jetpack-whatsapp');
    }
}
add_filter( 'wp_print_scripts', 'deregister_scripts' );

/*Deregister Contact Form 7 styles
================================================================================================================*/

//FUNÇÃO PARA SUPORTE A UPLOAD DE IMAGENS SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

include(TEMPLATEPATH . '/template-parts/walker.php');

// // Aumenta limite de upload
// @ini_set( 'upload_max_size' , '64M' );
// @ini_set( 'post_max_size', '64M');
// @ini_set( 'max_execution_time', '300' );


//add_filter( 'jetpack_development_mode', '__return_true' );

//ADICIONA SUPORTE A MENUS NO PAINEL ADMIN
add_theme_support('menus');
//ADICIONA SUPORTE POST FORMATS
add_theme_support( 'post-formats', array( 'video' ) );
//REMOVE BARRA DE ADMIN
add_filter('show_admin_bar', '__return_false');
//ENABLE THUMBS
add_theme_support('post-thumbnails');
//UNABLE LOGIN SHOW ERRORS
add_filter('login_errors',create_function('$a', "return null;"));
//REMOVE HEAD VERSION
remove_action('wp_head', 'wp_generator');
//ENABLE THUMBS
add_theme_support( 'post-thumbnails' );


// function tweakjp_rm_comments_att( $open, $post_id ) {
//   $post = get_post( $post_id );
//   if( $post->post_type == 'attachment' ) {
//       return false;
//   }
//   return $open;
// }
// add_filter( 'comments_open', 'tweakjp_rm_comments_att', 10 , 2 );

require_once('wp_bootstrap_navwalker.php');
//require_once('wlaker_dropdown.php');
register_nav_menus(
  array(
    'header'    => __('Header'),
    'footer' => __('Footer')
  )
);



/*=======================================================================================
CUSTOM FUNCTION: LIMIT TEXT
=======================================================================================*/
function the_content_limit($max_char, $conteudo = 'the_content', $more_link_text = '<br/><i class="fa fa-smile-o"></i>', $stripteaser = 0, $more_file = '') {
    $content = get_the_content($more_link_text, $stripteaser, $more_file);
    $content = apply_filters($conteudo, $content);
    $content = str_replace(']]>', ']]>', $content);

   if (strlen($_GET['']) > 0) {
      echo $content;
   }
   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {
      $content = substr($content, 0, $espacio);
      $content = $content;
      echo $content;
      echo '...';
   }
   else {
      echo $content;
   }
}

//CUSTOM POST TYPE
// add_action('init', 'type_post_solucoes');
// function type_post_solucoes() {
//   $labels = array(
//     'name'                => _x( 'Soluções', 'Post Type General Name', 'soluções' ),
//     'singular_name'       => _x( 'Solução', 'Post Type Singular Name', 'solução' ),
//     'menu_name'           => __( 'Soluções', 'solução' ),
//     'parent_item_colon'   => __( 'Parent Item:', 'Soluções' ),
//     'all_items'           => __( 'Todos as soluções', 'soluções' ),
//     'view_item'           => __( 'Ver Solução', 'solução' ),
//     'add_new_item'        => __( 'Add nova Solução', 'solução' ),
//     'add_new'             => __( 'Add nova', 'solução' ),
//     'edit_item'           => __( 'Editar Solução', 'solução' ),
//     'update_item'         => __( 'Atualizar Solução', 'solução' ),
//     'search_items'        => __( 'Pesquisar Solução', 'solução' ),
//     'not_found'           => __( 'Nada encontrado', 'solução' ),
//     'not_found_in_trash'  => __( 'Nada encontrado na lixeira', 'solução' ),
//     );
//   $rewrite = array(
//     'slug'                => 'solucoes'
//     );
//   $args = array(
//     'labels' => $labels,
//     'public' => true,
//     'public_queryable' => true,
//     'show_ui' => true,
//     'query_var' => true,
//     'rewrite' => $rewrite,
//     'capability_type' => 'post',
//     'has_archive' => true,
//     'menu_icon' => 'dashicons-admin-users',
//     'hierarchical' => false,
//     'menu_position' => null,

//     'supports' => array('title', 'editor', 'thumbnail', 'comments', 'revisions', 'trackbacks')
//     );

//     register_post_type( 'solucoes' , $args );
//     flush_rewrite_rules();
// }

// TAXONOMY PARA SOLUÇÔES
// function solucoes_taxonomy() {
//   register_taxonomy( 'categorias', 'solucoes',
//     array(
//       'labels' => array(
//           'name'              => 'Categorias de soluções',
//           'singular_name'     => 'Categoria de solução',
//           'search_items'      => 'Pesquisar Categorias de soluções',
//           'all_items'         => 'Todas as Categorias',
//           'edit_item'         => 'Editar Categorias',
//           'update_item'       => 'Atualizar Categorias',
//           'add_new_item'      => 'Adicionar nova Categoria',
//           'new_item_name'     => 'Nome da Categoria',
//           'menu_name'         => 'Categorias',
//       ),
//       'hierarchical' => true,
//       'supports'            => array( 'thumbnail' ),
//       'show_admin_column' => true
//     )
//   );
// }
// add_action( 'init', 'solucoes_taxonomy', 0);



/*ADICIONA FAVICON À TELA DE LOGIN E AO PAINEL DO SITE*/
add_action( 'login_head', 'favicon_admin' );
add_action( 'admin_head', 'favicon_admin' );
add_filter( 'wpcf7_support_html5', '__return_false' );
function favicon_admin() {
    $favicon_url = THEMEURL . '/assets/img/favicon';
    $favicon  = '<!-- Favicon IE 9 -->';
    $favicon .= '<!--[if lte IE 9]><link rel="icon" type="image/x-icon" href="' . $favicon_url . '.ico" /> <![endif]-->';
    $favicon .= '<!-- Favicon Outros Navegadores -->';
    $favicon .= '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '.png" />';
    $favicon .='<!-- Favicon iPhone -->';
    $favicon .='<link rel="apple-touch-icon" href="' . $favicon_url . '.png" />';
    echo $favicon;
}
/*=======================================================================================
STYLE FOR LOGIN PAGE
=======================================================================================*/
//Link na tela de login para a página inicial
// Custom WordPress Login Logo
function my_login_logo() {
  echo '
    <style type="text/css">
      html{
        background-color: #000;
      }
      .login #login_error{
        display: none;
      }
      body.login div#login h1 a {
        pointer-events: none;
        background-image: url(wp-content/themes/visual-sistemas-eletronicos/assets/img/visual-logo.png);
        padding-bottom: 0px;
        background-size: contain;
        width: 270px;
      }
      body.login {
        background-image: url(wp-content/themes/visual-sistemas-eletronicos/assets/img/visual-login.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: 50%;
      }
      .login form {
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
        background:#FFF;
        opacity:0.90;
        border-radius:5px;
      }
      .login #backtoblog a, .login #nav a {
        text-decoration: none;
        color: #FFF !important;
        font-weight:bold;
      }
      .login label {
        color: #0072bc !important;
        text-transform: uppercase;
        font-size: 14px;
      }
      .login input{
        -webkit-transition: all .3s ease-in-out;
        -moz-transition: all .3s ease-in-out;
        -ms-transition: all .3s ease-in-out;
        -o-transition: all .3s ease-in-out;
        transition: all .3s ease-in-out;
      }
      .wp-core-ui .button.button-large {
        height: 51px !important;
        line-height: 28px;
        margin-top:15px;
        color: #FFF;
        border-color: transparent !important;
        padding: 0px 12px 2px;
        width: 100%;
        background: #0072bc none repeat scroll 0% 0% !important;
        border-radius: 0px;
        text-transform: uppercase;
      }
      .wp-core-ui .button.button-large:hover{
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18),0 4px 15px 0 rgba(0,0,0,0.15);
      }
      .login input[type=text]:focus,
      .login input[type=password]:focus{
        -webkit-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        -moz-box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
        box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.26);
      }
    </style>
  ';
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );




/*MAIS LIDAS
======================*/
// Verifica se não existe nenhuma função com o nome post_count_session_start
if ( ! function_exists( 'post_count_session_start' ) ) {
    // Cria a função
    function post_count_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }
    // Executa a ação
    add_action( 'init', 'post_count_session_start' );
}

// Verifica se não existe nenhuma função com o nome tp_count_post_views
if ( ! function_exists( 'tp_count_post_views' ) ) {
    // Conta os views do post
    function tp_count_post_views () {
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {

            // Precisamos da variável $post global para obter o ID do post
            global $post;

            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {

                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;

                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );

                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor

            } // Checa a sessão

        } // is_single
        return;
      }
    add_action( 'get_header', 'tp_count_post_views' );
}





