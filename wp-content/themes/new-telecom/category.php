<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>
<div class="home-blog">
    <div class="my-container">
        <div class="row">
            <div class="hidden-xs col-sm-6 col-md-4 col-lg-3 side-desk">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
                <div class="row destaque" itemscope itemtype="http://schema.org/BlogPosting">
                    <?php
                    if(have_posts()) : while( have_posts() ) {
                        the_post();?>
                        <div class="itemPost">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 thumb pull-right">
                                <?php
                                $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); ?>
                                <a href="<?php echo the_permalink(); ?>">
                                <?php if ($img_post[0]){ ?>
                                    <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                                <?php }else{ ?>
                                    <img class="img-responsive" src="<?php echo THEMEURL; ?>/assets/img/default.jpg" alt="<?php the_title(); ?>">
                                <?php } ?>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                                <div class="titulo-destaque">
                                    <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                    <?php
                                    $categoria = get_the_category();
                                    foreach($categoria as $category) {
                                        $output = '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
                                    }?>
                                    <p class="data" ><?php the_date('d/m/Y'); ?> <i class="icon-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
                                </div>
                                <div class="desc-destaque">
                                    <p><a href="<?php echo the_permalink(); ?>"><?php the_content_limit(200); ?></a></p>
                                    <div class="know"><a href="<?php echo the_permalink(); ?>">Ler Mais</a></div>
                                </div>
                            </div>
                        </div>
                    <?php } endif;?>
                    <div class="navegacao">
                    <?php the_posts_pagination( array(
                        'mid_size' => 2,
                        'prev_text' => __( '<i class="icon-angle-left"></i>'),
                        'next_text' => __( '<i class="icon-angle-right"></i>'),
                        ) );
                    ?>
                    </div>
                </div>
            </div>
            <div class="hidden-xs col-sm-6 col-md-4 col-lg-3 side-mobile">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>