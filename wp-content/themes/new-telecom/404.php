<?php get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="the-error">
	<div class="my-container">
		<div class="row">
			<div class="col-xs-12">
				<div class="search404">
					<h2>Infelizmente, não encontramos essa página.</h2>
					<div class="error">
						<p>Talvez o menu acima tenha o que você procura!</p>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>
<?php get_footer(); ?>