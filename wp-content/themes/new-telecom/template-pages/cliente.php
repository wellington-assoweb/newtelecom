<?php get_header();  /* Template name: Clientes */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<div class="clientes">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-lg-10 centering">
                <p><?php echo get_field('chamada'); ?></p>
                <?php
                    while(have_rows('clientes')): the_row();
                        $imagem = get_sub_field('imagem');
                ?>
                        <div class="cliente">
                            <img src="<?php echo $imagem['url']; ?>" alt="<?php echo $imagem['alt']; ?>" title="<?php echo $imagem['title']; ?>">
                        </div>
                <?php $count++; endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>







