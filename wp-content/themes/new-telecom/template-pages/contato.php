<?php get_header();  /* Template name: Contato */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="contato">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-8 centering">
                <h2><?php echo get_field('titulo'); ?></h2>
                <?php echo get_field('sub_titulo'); ?>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4 centering">
                <?php echo do_shortcode('[contact-form-7 id="121" title="Contato"]'); ?>
                <div class="email-tel">
                    <div>contato@onlinerastreamento.com.br</div>
                    <div>31 3623-2190</div>
                    <div>31 99149-2402</div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
<script>
</script>







