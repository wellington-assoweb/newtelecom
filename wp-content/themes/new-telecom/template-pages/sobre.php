<?php get_header(); /* Template name: Sobre */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="box-container" data-speed="10" style="background-image:url('<?php echo THEMEURL ?>/assets/img/carros.jpg')">
	<img class="icone-fixed-logo" data-speed="3" src="<?php echo THEMEURL ?>/assets/img/logo-icone-vermelho.svg" alt="">
	<img class="icone-fixed-logo traco" data-speed="3" src="<?php echo THEMEURL ?>/assets/img/icone-vermelho-traco.svg" alt="">
	<div class="a-online">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-md-8 centering texto">
					<h2><?php echo get_field('titulo_chamada_principal'); ?></h2>
					<?php echo get_field('conteudo_chamada_principal'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="missao">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-md-8 centering texto">
					<h3>Missão</h3>
					<?php echo get_field('missao'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="visao">
		<div class="my-container">
			<div class="row">
				<div class="col-xs-12 col-md-8 centering texto">
					<h3>Visão</h3>
					<?php echo get_field('vision'); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="valores">
		<div class="my-container">
			<div class="row texto">
				<div class="col-xs-12">
					<h3>Valores</h3>
					<p><?php echo get_field('valores'); ?></p>
				</div>
				<? 
					while(have_rows('itens_valores')): the_row();
						$titulo_item = get_sub_field('titulo_item');
						$conteudo_item = get_sub_field('conteudo_item');
				?>
						<div class="col-xs-12 col-md-4">
							<h4><?php echo $titulo_item; ?></h4>
							<p><?php echo $conteudo_item; ?></p>
						</div>
					<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<script>
(function($) {
	$(document).ready(function(){
      var $window = $(window);

      	var $width = $window.width();
      	if($width > 900){
	        $('.icone-fixed-logo').each(function(){
	            var $bgobj = $(this);
	            $(window).scroll(function() {
	                var yPos = -($window.scrollTop() / $bgobj.data('speed'));
	                var coords = yPos+200 + 'px';
	                $bgobj.css({ 
	                    top: coords 
	                });
	            });
	        });
		}else{
			$('.icone-fixed-logo').each(function(){
	            var $bgobj = $(this);
	            $(window).scroll(function() {
	                var yPos = -($window.scrollTop() / $bgobj.data('speed'));
	                var coords = yPos+100 + 'px';
	                $bgobj.css({ 
	                    top: coords 
	                });
	            });
	        });
		}
        $('.box-container').each(function(){
            var $bgobj = $(this);
            $(window).scroll(function() {
                var yPos = ($window.scrollTop() / $bgobj.data('speed'));
                var coords = '50% '+ yPos + 'px';
                $bgobj.css('background-position', coords );
            });
        });
    });
})(jQuery);
</script>