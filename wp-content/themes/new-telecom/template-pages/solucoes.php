<?php get_header();  /* Template name: Soluções */  ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-comum.php'); ?>

<section class="header-solucoes">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-10 col-lg-7 centering">
                    <?php
                        $count = 1;
                        while(have_rows('solucoes')): the_row();
                            $icone = get_sub_field('icone');
                            $titulo = get_sub_field('titulo');

                    ?>
                            <div class="item" data-id="<?php echo $count ?>">
                                <img src="<?php echo $icone['url']; ?>" alt="<?php echo $icone['alt']; ?>" alt="<?php echo $icone['title']; ?>">
                                <div><?php echo $titulo ?></div>
                            </div>
                    <?php $count++; endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>



<section class="solucoes">
    <?php
        $count = 1;
        while(have_rows('solucoes')): the_row();
            $imagem = get_sub_field('imagem');
            $titulo = get_sub_field('titulo');
            $conteudo = get_sub_field('conteudo');
    ?>
        <div class="box" id="<?php echo $count; ?>" style="background-image:url('<?php echo $imagem['url'] ?>');">
            <div class="my-container">
                <div class="row">
                    <div class="texto">
                        <h3><?php echo $titulo; ?></h3>
                        <div class="desc">
                            <?php echo $conteudo; ?>
                        </div>
                    </div>
                    <div class="botao btn-roxo">
                        <a href="<?php echo SITEURL ?>/contato"><span>CONTRATAR</span></a>
                    </div>
                </div>
            </div>
        </div>
    <?php $count++; endwhile; wp_reset_query(); ?>
</section>
<section class="mapa" style="background-image:url('<?php echo THEMEURL?>/assets/img/mapa-do-brasil-online-rastreamento.jpg');">
    <div class="my-container">
        <div class="row">
            <div class="col-xs-12 col-md-10 centering">
                <h3><?php echo get_field('titulo_ultimo_box') ?></h3>
                <?php echo get_field('conteudo_ultimo_box') ?>
                <div class="botao btn-branco"><a href="<?php echo SITEURL ?>/solucoes"><span>CONTRATAR</span></a></div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
<script>
(function($) {
    jQuery(document).ready(function(){
        $('.item').click(function(){
            $('.box.show').removeClass('show');
            $('.item.green-img').removeClass('green-img');
            $this = $(this);
            $this.addClass('green-img');
            dataId = $this.attr('data-id');
            $('#'+dataId).addClass('show');
        });
    });
})(jQuery);
</script>







