<?php /* Template name: Blog */
get_header(); ?>
<?php include(TEMPLATEPATH . '/template-parts/titulo-blog.php'); ?>
<div class="home-blog">
	<div class="my-container">
		<div class="row">
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 side-desk">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                <div class="row destaque" itemscope itemtype="http://schema.org/BlogPosting">
                <!-- DESTAQUE -->
                <?php
                $args = array(
                    'posts_per_page' => 1,
                    'post__in'  => get_option( 'sticky_posts' ),
                    'ignore_sticky_posts' => 1
                );
                $destaque_query = new WP_Query( $args );
                $id = $post->ID;

                if($destaque_query->have_posts()) : while( $destaque_query->have_posts() ) {
                    $destaque_query->the_post();?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7 thumb pull-right">
                        <?php
                        $img_post = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' ); ?>
                        <a href="<?php echo the_permalink(); ?>">
                        <?php if ($img_post[0]){ ?>
                            <img class="img-responsive" src="<?php echo $img_post[0] ?>" alt="<?php the_title(); ?>">
                        <?php }else{ ?>
                            <img class="img-responsive" src="<?php echo THEMEURL; ?>/assets/img/default.jpg" alt="<?php the_title(); ?>">
                        <?php } ?>

                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                        <div class="titulo-destaque">
                            <h3 itemprop="headline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <?php
                            $categoria = get_the_category();
                            foreach($categoria as $category) {
                                $output = '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>';
                            }?>
                            <p class="data" ><?php the_date('d/m/Y'); ?> <i class="icon-dot"></i> <?php echo $output; ?><span itemprop="author" class="hidden"><?php the_author_posts_link(); ?></span></p>
                        </div>
                        <div class="desc-destaque">
                            <p><a href="<?php echo the_permalink(); ?>"><?php the_content_limit(200); ?></a></p>
                            <div class="know"><a href="<?php echo the_permalink(); ?>">Ler Mais</a></div>
                        </div>
                    </div>
                <?php } endif;?>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <h3>Últimas notícias</h3>
                    </div>
                </div>
                <div class="row ultimas">
                <!-- DESTAQUE -->
                <?php
                    $args2 = array(
                        'posts_per_page' => 3,
                        'post__not_in' => array($id)
                    );
                    $ultimos_posts = new WP_Query( $args2 );
                    $id2 = array();

                    if($ultimos_posts->have_posts()) : while( $ultimos_posts->have_posts() ) {
                        $ultimos_posts->the_post();?>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            <div class="itemArticle">
                                <?php
                                    $img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                                ?>
                                <figure class="effect-lily">
                                    <?php if ($img_post_art[0]){?>
                                        <img src="<?php echo $img_post_art[0] ?>" alt="<?php the_title(); ?>"/>
                                    <?php }else{?>
                                        <img src="<?php echo THEMEURL?>/assets/img/default.jpg" alt="<?php the_title(); ?>"/>
                                    <?php }?>
                                    <figcaption>
                                        <div>
                                            <h4><?php the_title(); ?></h4>
                                            <p>Ler mais</p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>">View more</a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    <?php
                    $id2 .= $post->ID;
                    } endif;?>
                    <div class="col-lg-12">
                        <div class="divider1"></div>
                    </div>
                </div>

                <?php
                $args2 = array(
                    'posts_per_page' => 3,
                    'post__not_in' => array($id),
                    'ignore_sticky_posts' => true,              // Ignora posts fixos
                    'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
                    'meta_key'            => 'tp_post_counter', // A nossa post meta
                    'order'               => 'DESC'             // Ordem decrescente

                );
                $ultimos_posts = new WP_Query( $args2 );

                if($ultimos_posts->have_posts()){?>
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Mais lidas</h3>
                    </div>
                </div>
                <div class="row mais-lidas">

                    <?php while( $ultimos_posts->have_posts() ) {
                    $ultimos_posts->the_post();?>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <div class="itemArticle">
                        <?php
                            $img_post_art = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium' );
                        ?>
                            <figure class="effect-lily">
                                <?php if ($img_post_art[0]){?>
                                    <img src="<?php echo $img_post_art[0] ?>" alt="<?php the_title(); ?>"/>
                                <?php }else{?>
                                    <img src="<?php echo THEMEURL?>/assets/img/default.jpg" alt="<?php the_title(); ?>"/>
                                <?php }?>
                                <figcaption>
                                    <div>
                                        <h4><?php the_title(); ?></h4>
                                        <p>Ler mais</p>
                                    </div>
                                    <a href="<?php the_permalink(); ?>">View more</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <?php }?>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 side-mobile">
                <?php include(TEMPLATEPATH . '/template-parts/sidebar-left.php'); ?>
            </div>
		</div>
	</div>
</div>

<?php get_footer(); ?>