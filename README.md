# Assoweb WordPress Theme base ([Gulp](http://gulpjs.com) version)

Base structure for front end project together with Gulpjs

## Quick start

1. Clone the Bitbucket repo - `git clone https://bitbucket.org/agenciaassoweb/base`;
2. Change the following reference:
2a. Name of the home directory
2b. theme directory
2c. `THEMEURL` and `SITEURL` constants in `wp-config.php`;
4. Enter in WordPress project theme - `localhost/project_name/wp-content/themes/theme_name`;
5. Install dependences - `npm install`;
6. Enjoy!

## Features

* Gulp ready;
* Browser sync;
* Placeholder crossbrowser ready;
* Comes with twitter bootstrap Sass;

*You must have a server running Apache (as WAMP, WAMP, LAMP or native). You should also have installed npm, in order to install the dependencies.*

## Follow-us
* [Facebook](https://www.facebook.com/agenciaassoweb)
* [Twitter](https://twitter.com/agenciaassoweb	)
* [Instagram](https://www.instagram.com/agenciaassoweb/)
* [Google Plus](https://plus.google.com/+AssowebBr)
* [Pinterest](https://br.pinterest.com/agenciaassoweb/)